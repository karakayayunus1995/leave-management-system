package com.karakaya.permissionsystem.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.karakaya.permissionsystem.entity.User;
import com.karakaya.permissionsystem.model.AuthRequest;
import com.karakaya.permissionsystem.model.UserRequest;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@SpringBootTest
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@RequiredArgsConstructor
class UserControllerTest {

    private final static String URL = "http://localhost:8080/user/";
    private static String token = "";
    @Autowired
    private MockMvc mvc;

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @Order(1)
    void create() throws Exception {
        UserRequest request = new UserRequest("ADMIN", "TestAdmin", "password", LocalDate.of(2018, 5, 3));
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .post(URL + "register")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists()).andReturn();
    }

    @Test
    @Order(2)
    void login() throws Exception {
        AuthRequest request = new AuthRequest("TestAdmin", "password");
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .post(URL + "login")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.token").exists()).andReturn();

        String json = result.getResponse().getContentAsString();
        User user = new ObjectMapper().readValue(json, User.class);
        token = user.getToken();
    }

    @Test
    @Order(3)
    void userMe() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .get(URL + "me").header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists()).andReturn();
        String json = result.getResponse().getContentAsString();
        User user = new ObjectMapper().readValue(json, User.class);
        assertEquals(user.getUsername(), "TestAdmin");
    }

    @Test
    @Order(4)
    void userDetail() throws Exception {
        MvcResult result = mvc.perform(MockMvcRequestBuilders
                        .get(URL + "TestAdmin").header("Authorization", "Bearer " + token)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").exists()).andReturn();
        String json = result.getResponse().getContentAsString();
        User user = new ObjectMapper().readValue(json, User.class);
        assertEquals(user.getUsername(), "TestAdmin");
    }


}