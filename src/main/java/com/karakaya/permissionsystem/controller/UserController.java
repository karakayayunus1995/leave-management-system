package com.karakaya.permissionsystem.controller;

import com.karakaya.permissionsystem.entity.User;
import com.karakaya.permissionsystem.mapper.UserMapper;
import com.karakaya.permissionsystem.model.AuthRequest;
import com.karakaya.permissionsystem.model.UserRequest;
import com.karakaya.permissionsystem.model.dto.UserDto;
import com.karakaya.permissionsystem.security.JwtTokenUtil;
import com.karakaya.permissionsystem.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Tag(name = "User", description = "Operations about user")
@RequestMapping("user")
@Validated
@RequiredArgsConstructor
public class UserController {

    private final UserMapper userMapper;
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @Operation(summary = "Create New User ", description = "Do Not Need Authenticated. System Role are 'ADMIN' and 'EMPLOYEE'")
    public ResponseEntity<UserDto> create(@Valid @RequestBody UserRequest request) {
        User user = userMapper.requestToUser(request);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return new ResponseEntity<>(userMapper.userToDto(userService.create(user)), new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    @Operation(summary = "Login To System.",
            description = "Do Not Need Authenticated. Do Not Forget To Add The Token From The Reply To The Header")
    public ResponseEntity<UserDto> login(@RequestBody @Valid AuthRequest request) {
        try {
            Authentication authenticate = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword()));

            User user = (User) authenticate.getPrincipal();
            String token = jwtTokenUtil.generateAccessToken(user);
            user.setToken(token);
            return ResponseEntity.ok()
                    .header(HttpHeaders.AUTHORIZATION, token)
                    .body(userMapper.userToDto(user));
        } catch (BadCredentialsException ex) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }


    @PreAuthorize("hasAuthority (T(com.karakaya.permissionsystem.constant.UserRole).ADMIN) " +
            "|| hasAuthority (T(com.karakaya.permissionsystem.constant.UserRole).EMPLOYEE)")
    @RequestMapping(value = "me", method = RequestMethod.GET)
    @Operation(summary = "Your Own User Information",
            description = "Retrieves The Authenticated User's Information. Need 'ADMIN' or 'EMPLOYEE' Role")
    public ResponseEntity<UserDto> userMe() {
        return new ResponseEntity<>(userMapper.userToDto(userService.getUser()), new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority (T(com.karakaya.permissionsystem.constant.UserRole).ADMIN) ")
    @RequestMapping(value = "{username}", method = RequestMethod.GET)
    @Operation(summary = "Any User Information",
            description = "Fetches Information Of Any Registered User by Username. Need 'ADMIN' Role")
    public ResponseEntity<UserDto> userDetail(@PathVariable String username) {
        return new ResponseEntity<>(userMapper.userToDto(userService.userFindByUsername(username)),
                new HttpHeaders(), HttpStatus.OK);
    }

}
