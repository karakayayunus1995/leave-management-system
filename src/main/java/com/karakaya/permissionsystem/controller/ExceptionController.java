package com.karakaya.permissionsystem.controller;

import com.karakaya.permissionsystem.exception.CustomRestException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = "Exception", description = "Try Exception for Localization Message")
@RequestMapping("exception")
public class ExceptionController {

    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Create New Exception ", description = "It was made for trial purposes for testing.")
    @Parameter(
            name = "Accept-Language",
            in = ParameterIn.HEADER,
            allowEmptyValue = false,
            schema = @Schema(type = "string", allowableValues = {"tr", "en"})
    )
    public void generateException() {
        throw new CustomRestException("message.CustomRestException");
    }
}
