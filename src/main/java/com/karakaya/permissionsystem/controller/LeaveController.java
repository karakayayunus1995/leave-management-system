package com.karakaya.permissionsystem.controller;

import com.karakaya.permissionsystem.constant.LeaveStatus;
import com.karakaya.permissionsystem.entity.Leave;
import com.karakaya.permissionsystem.exception.LeaveChangeStatusException;
import com.karakaya.permissionsystem.mapper.LeaveMapper;
import com.karakaya.permissionsystem.model.LeaveRequest;
import com.karakaya.permissionsystem.model.PagingModel;
import com.karakaya.permissionsystem.service.LeaveService;
import com.karakaya.permissionsystem.service.UserService;
import com.karakaya.permissionsystem.specification.LeaveSpecification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Tag(name = "Leave", description = "Operations about Leaves")
@RequestMapping("leave")
@Validated
@RequiredArgsConstructor
public class LeaveController {

    private final LeaveService leaveService;
    private final UserService userService;
    private final LeaveMapper leaveMapper;

    @PreAuthorize("hasAuthority (T(com.karakaya.permissionsystem.constant.UserRole).ADMIN) " +
            "|| hasAuthority (T(com.karakaya.permissionsystem.constant.UserRole).EMPLOYEE)")
    @RequestMapping(method = RequestMethod.POST)
    @Operation(summary = "Create New Leave For Authenticated  User ", description = "Need 'ADMIN' or 'EMPLOYEE' Role")
    public ResponseEntity<Leave> create(@Valid @RequestBody LeaveRequest request) {
        Leave leave = leaveMapper.requestToLeave(request);
        leave.setUser(userService.getUser());
        return new ResponseEntity<>(leaveService.create(leave), new HttpHeaders(), HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority (T(com.karakaya.permissionsystem.constant.UserRole).ADMIN)")
    @RequestMapping(value = {"{id}"}, method = RequestMethod.PATCH)
    @Parameter(
            name = "status",
            in = ParameterIn.QUERY,
            allowEmptyValue = false,
            schema = @Schema(type = "string", allowableValues = {"ACCEPTED", "REJECTED"})
    )
    @Operation(summary = "Accept Or Reject Leave",
            description = "Need ADMIN role. Can Just Change The Leave Which Is Status PENDING")
    public ResponseEntity<Leave> changeStatus(@RequestParam String status,
                                              @PathVariable Long id) {
        Leave leave = leaveService.getLeave(id);
        if (!leave.getStatus().equals(LeaveStatus.PENDING.name())) {
            throw new LeaveChangeStatusException("message.LeaveChangeStatusException");
        }
        leave.setStatus(status);
        return new ResponseEntity<>(leaveService.changeStatus(leave), new HttpHeaders(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority (T(com.karakaya.permissionsystem.constant.UserRole).ADMIN)")
    @RequestMapping(method = RequestMethod.GET)
    @Parameter(
            name = "status",
            in = ParameterIn.QUERY,
            allowEmptyValue = false,
            schema = @Schema(type = "string", allowableValues = {"PENDING", "ACCEPTED", "REJECTED"})
    )
    @Operation(summary = "List All Leaves With Paging", description = "Need 'ADMIN' Role")
    public ResponseEntity<PagingModel<Leave>> getAllLeave(
            @RequestParam(value = "page", name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "size", name = "size", required = false, defaultValue = "15") int size,
            @RequestParam(value = "status", name = "status", required = false, defaultValue = "PENDING") String status

    ) {
        LeaveSpecification specification = new LeaveSpecification(status);
        Page<Leave> pageData = leaveService.getAllLeave(page, size, specification);
        PagingModel<Leave> response = new PagingModel<>(pageData.getTotalElements(),
                pageData.getTotalPages(), pageData.getNumber(), pageData.getContent());
        return new ResponseEntity<>(response, new HttpHeaders(), HttpStatus.OK);
    }
}
