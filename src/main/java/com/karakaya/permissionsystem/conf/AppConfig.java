package com.karakaya.permissionsystem.conf;

import com.karakaya.permissionsystem.mapper.LeaveMapper;
import com.karakaya.permissionsystem.mapper.UserMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public UserMapper userMapper() {
        return UserMapper.INSTANCE;
    }

    @Bean
    public LeaveMapper leaveMapper() {
        return LeaveMapper.INSTANCE;
    }


}
