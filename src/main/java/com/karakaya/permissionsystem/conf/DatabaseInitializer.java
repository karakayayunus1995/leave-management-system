package com.karakaya.permissionsystem.conf;

import com.karakaya.permissionsystem.constant.UserRole;
import com.karakaya.permissionsystem.entity.PublicHoliday;
import com.karakaya.permissionsystem.entity.User;
import com.karakaya.permissionsystem.service.PublicHolidayService;
import com.karakaya.permissionsystem.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DatabaseInitializer implements ApplicationListener<ApplicationReadyEvent> {

    private final PublicHolidayService publicHolidayService;
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;


    private List<PublicHoliday> collectPublicHolidayDates() {

        List<PublicHoliday> publicHolidays = new ArrayList<>();
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 1, 1), "Yılbaşı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 4, 23), "Ulusal Egemenlik ve Çocuk Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 5, 1), "Emek ve Dayanışma Günü"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 5, 2), "Ramazan Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 5, 3), "Ramazan Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 5, 4), "Ramazan Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 5, 19), "Atatürk'ü Anma, Gençlik ve Spor Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 7, 9), "Kurban Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 7, 10), "Kurban Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 7, 11), "Kurban Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 7, 12), "Kurban Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 7, 15), "Demokrasi Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 8, 30), "Zafer Bayramı"));
        publicHolidays.add(new PublicHoliday(null, LocalDate.of(2022, 10, 29), "Cumhuriyet Bayramı"));
        return publicHolidays;
    }

    private void createInitialUser() {
        String password = passwordEncoder.encode("password");
        userService.create(new User(null, UserRole.ADMIN.name(), "Admin", password, LocalDate.of(2012, 1, 20), 0, null, null, null, ""));
        userService.create(new User(null, UserRole.EMPLOYEE.name(), "Employee", password, LocalDate.of(2019, 1, 20), 0, null, null, null, ""));
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        List<PublicHoliday> holidays = collectPublicHolidayDates();
        publicHolidayService.saveAll(holidays);
        createInitialUser();
    }
}
