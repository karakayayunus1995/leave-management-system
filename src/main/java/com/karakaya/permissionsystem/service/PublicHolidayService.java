package com.karakaya.permissionsystem.service;

import com.karakaya.permissionsystem.entity.PublicHoliday;
import com.karakaya.permissionsystem.repository.PublicHolidayDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PublicHolidayService {

    private final PublicHolidayDao publicHolidayDao;

    public List<PublicHoliday> saveAll(List<PublicHoliday> holidays) {
        return publicHolidayDao.saveAll(holidays);
    }

    public List<PublicHoliday> getAll() {
        return publicHolidayDao.findAll();
    }
}
