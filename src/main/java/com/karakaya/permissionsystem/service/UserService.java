package com.karakaya.permissionsystem.service;

import com.karakaya.permissionsystem.constant.LeaveStatus;
import com.karakaya.permissionsystem.constant.UserRole;
import com.karakaya.permissionsystem.entity.Leave;
import com.karakaya.permissionsystem.entity.User;
import com.karakaya.permissionsystem.exception.UnAcceptedRoleException;
import com.karakaya.permissionsystem.exception.UserAlreadyExistException;
import com.karakaya.permissionsystem.repository.UserDao;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserDao userDao;

    public User create(User user) {
        checkUserExist(user.getUsername());
        checkUserRole(user.getRole());
        int yearWorked = calculateYearWorked(user.getJobStartDate());
        user.setTotalLeave(calculateLeaveDay(yearWorked));
        return userDao.save(user);
    }

    private void checkUserExist(String username) {
        if (userDao.findByUsername(username).isPresent()) {
            throw new UserAlreadyExistException("message.UserAlreadyExistException");
        }
    }

    public User userFindByUsername(String username) {
        return userDao.findByUsername(username).orElseThrow(
                () -> new UsernameNotFoundException("User: " + username + ", not found"));
    }

    private static int calculateYearWorked(LocalDate jobStartDate) {
        if (jobStartDate != null) {
            return Period.between(jobStartDate, LocalDate.now()).getYears();
        } else {
            return 0;
        }
    }

    private static int calculateLeaveDay(int yearWorked) {
        if (yearWorked == 0) {
            return 5;
        } else if (isBetween(yearWorked, 1, 5)) {
            return 15;
        } else if (isBetween(yearWorked, 6, 10)) {
            return 18;
        } else if (yearWorked > 10) {
            return 24;
        } else {
            return 0;
        }
    }

    private static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }


    private void checkUserRole(String role) {
        if (!role.equals(UserRole.ADMIN.name()) && !role.equals(UserRole.EMPLOYEE.name())) {
            throw new UnAcceptedRoleException("message.UnAcceptedRoleException");
        }
    }

    public User getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        return userFindByUsername(user.getUsername());
    }

    public void updateUserTotalLeave(Leave leave) {
        User user = leave.getUser();
        if (leave.getStatus().equals(LeaveStatus.REJECTED.name())) {
            user.setTotalLeave(user.getTotalLeave() + leave.getLeaveDay());
        } else if (leave.getStatus().equals(LeaveStatus.PENDING.name())) {
            user.setTotalLeave(user.getTotalLeave() - leave.getLeaveDay());
        }
        userDao.save(user);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userFindByUsername(username);
//        Collection<GrantedAuthority> authorities = new ArrayList<>();
//        authorities.add(new SimpleGrantedAuthority(user.getRole()));
        return new org.springframework.security.core.userdetails
                .User(user.getUsername(), user.getPassword(), Collections.emptyList());
    }
}
