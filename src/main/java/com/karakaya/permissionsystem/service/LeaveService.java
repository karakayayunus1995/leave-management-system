package com.karakaya.permissionsystem.service;

import com.karakaya.permissionsystem.constant.LeaveStatus;
import com.karakaya.permissionsystem.entity.Leave;
import com.karakaya.permissionsystem.entity.PublicHoliday;
import com.karakaya.permissionsystem.exception.LeaveDateNotAcceptedException;
import com.karakaya.permissionsystem.exception.LeaveNotFoundException;
import com.karakaya.permissionsystem.exception.LeaveStatusNotAcceptableException;
import com.karakaya.permissionsystem.repository.LeaveDao;
import com.karakaya.permissionsystem.specification.LeaveSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class LeaveService {

    private final LeaveDao leaveDao;
    private final UserService userService;
    private final PublicHolidayService publicHolidayService;

    public Leave create(Leave leave) {
        Set<LocalDate> acceptableDates = calculateAcceptLeaveDay(leave);
        leave.setLeaveDay(acceptableDates.size());
        leave.setAcceptableDates(writeAcceptableDates(acceptableDates));
        if (leave.getUser().getTotalLeave() < leave.getLeaveDay()) {
            throw new LeaveDateNotAcceptedException("message.LeaveDateNotAcceptedException");
        }
        leave.setUser(userService.getUser());
        leave.setStatus(LeaveStatus.PENDING.name());
        userService.updateUserTotalLeave(leave);
        return leaveDao.save(leave);
    }

    private Set<LocalDate> calculateAcceptLeaveDay(Leave leave) {
        List<PublicHoliday> holidays = publicHolidayService.getAll();
        List<LocalDate> dates = getDatesBetween(leave.getFromDate(), leave.getToDate().plusDays(1));
        Set<LocalDate> acceptableDates = new HashSet<>();
        for (LocalDate date : dates) {
            int numberOfWeek = getDayNumberOfWeek(date);
            if (numberOfWeek == 6 || numberOfWeek == 7 || isHolidayDay(holidays, date)) {
                continue;
            }

            acceptableDates.add(date);
        }
        return acceptableDates;
    }


    private boolean isHolidayDay(List<PublicHoliday> holidays, LocalDate date) {
        PublicHoliday result = holidays.stream()
                .filter(holiday -> date.equals(holiday.getDate()))
                .findFirst()
                .orElse(null);
        return result != null;
    }

    private String writeAcceptableDates(Set<LocalDate> acceptableDates) {
        StringBuilder response = new StringBuilder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        for (LocalDate date : acceptableDates) {
            response.append(date.format(formatter) + ", ");
        }
        return response.toString();
    }

    public static int getDayNumberOfWeek(LocalDate date) {
        DayOfWeek day = date.getDayOfWeek();
        return day.getValue();
    }

    public static List<LocalDate> getDatesBetween(
            LocalDate startDate, LocalDate endDate) {
        long numOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        return IntStream.iterate(0, i -> i + 1)
                .limit(numOfDaysBetween)
                .mapToObj(startDate::plusDays)
                .collect(Collectors.toList());
    }

    public Leave getLeave(Long id) {
        return leaveDao.findById(id).orElseThrow(() -> new LeaveNotFoundException("message.LeaveNotFoundException"));
    }

    public Leave changeStatus(Leave leave) {
        checkStatus(leave.getStatus());
        if (leave.getStatus().equals(LeaveStatus.REJECTED.name())) {
            userService.updateUserTotalLeave(leave);
        }
        return leaveDao.save(leave);
    }

    public void checkStatus(String status) {
        if (!status.equals(LeaveStatus.ACCEPTED.name())
                && !status.equals(LeaveStatus.REJECTED.name())) {
            throw new LeaveStatusNotAcceptableException("message.LeaveStatusNotAcceptableException");
        }
    }


    public Page<Leave> getAllLeave(int page, int size, LeaveSpecification specification) {
        Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "id"));
        return leaveDao.findAll(specification, pageable);
    }
}
