package com.karakaya.permissionsystem.exception;

import com.karakaya.permissionsystem.model.ApiError;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.Locale;

@ControllerAdvice
public class UserExceptionHandler extends BaseExceptionHandler {

    public UserExceptionHandler(MessageSource messageSource) {
        super(messageSource);
    }

    @ExceptionHandler(value = UnAcceptedRoleException.class)
    public ResponseEntity<ApiError> UnAcceptedRoleExceptionHandler(UnAcceptedRoleException exception, Locale locale) {

        ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, LocalDateTime.now().format(formatter),
                getLocalizeMessage(exception.getMessage(), locale));
        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(value = UserAlreadyExistException.class)
    public ResponseEntity<ApiError> UserAlreadyExistExceptionHandler(UserAlreadyExistException exception, Locale locale) {

        ApiError apiError = new ApiError(HttpStatus.NOT_ACCEPTABLE, LocalDateTime.now().format(formatter),
                getLocalizeMessage(exception.getMessage(), locale));
        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);
    }

}
