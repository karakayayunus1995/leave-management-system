package com.karakaya.permissionsystem.exception;

public class LeaveDateNotAcceptedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LeaveDateNotAcceptedException(String message) {
        super(message);
    }
}
