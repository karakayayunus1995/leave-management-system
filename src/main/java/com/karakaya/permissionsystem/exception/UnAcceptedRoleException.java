package com.karakaya.permissionsystem.exception;

public class UnAcceptedRoleException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public UnAcceptedRoleException(String message) {
        super(message);
    }
}
