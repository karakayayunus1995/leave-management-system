package com.karakaya.permissionsystem.exception;

public class LeaveStatusNotAcceptableException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LeaveStatusNotAcceptableException(String message) {
        super(message);
    }
}
