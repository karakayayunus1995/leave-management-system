package com.karakaya.permissionsystem.exception;

import com.karakaya.permissionsystem.model.ApiError;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@ControllerAdvice
public class LeaveExceptionHandler extends BaseExceptionHandler {

    public LeaveExceptionHandler(MessageSource messageSource) {
        super(messageSource);
    }

    @ExceptionHandler(value = LeaveNotFoundException.class)
    public ResponseEntity<ApiError> LeaveNotFoundExceptionHandler(LeaveNotFoundException exception, Locale locale) {

        ApiError apiError = new ApiError(HttpStatus.NOT_FOUND, LocalDateTime.now().format(formatter),
                getLocalizeMessage(exception.getMessage(), locale));
        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = LeaveStatusNotAcceptableException.class)
    public ResponseEntity<ApiError> LeaveStatusNotAcceptableExceptionHandler(LeaveStatusNotAcceptableException exception, Locale locale) {
        ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, LocalDateTime.now().format(formatter),
                getLocalizeMessage(exception.getMessage(), locale));
        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(value = LeaveDateNotAcceptedException.class)
    public ResponseEntity<ApiError> LeaveDateNotAcceptedExceptionHandler(LeaveDateNotAcceptedException exception, Locale locale) {

        ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, LocalDateTime.now().format(formatter),
                getLocalizeMessage(exception.getMessage(), locale));
        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(value = LeaveChangeStatusException.class)
    public ResponseEntity<ApiError> LeaveChangeStatusExceptionHandler(LeaveChangeStatusException exception, Locale locale) {

        ApiError apiError = new ApiError(HttpStatus.METHOD_NOT_ALLOWED, LocalDateTime.now().format(formatter),
                getLocalizeMessage(exception.getMessage(), locale));
        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.METHOD_NOT_ALLOWED);
    }


}
