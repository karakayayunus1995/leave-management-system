package com.karakaya.permissionsystem.exception;

public class CustomRestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CustomRestException(String message) {
        super(message);
    }
}
