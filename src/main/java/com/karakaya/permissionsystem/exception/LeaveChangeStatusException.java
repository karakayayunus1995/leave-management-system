package com.karakaya.permissionsystem.exception;

public class LeaveChangeStatusException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public LeaveChangeStatusException(String message) {
        super(message);
    }
}
