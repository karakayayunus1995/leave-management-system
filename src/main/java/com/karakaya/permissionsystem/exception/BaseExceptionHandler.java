package com.karakaya.permissionsystem.exception;

import com.karakaya.permissionsystem.model.ApiError;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@RequiredArgsConstructor
@ControllerAdvice
public class BaseExceptionHandler {

    protected final MessageSource messageSource;

    protected DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

    protected String getLocalizeMessage(String messageCode, Locale locale) {
        return messageSource.getMessage(
                messageCode, new Object[]{}, locale);
    }

    @ExceptionHandler(value = CustomRestException.class)
    public ResponseEntity<ApiError> CustomRestExceptionHandler(CustomRestException exception, Locale locale) {

        ApiError apiError = new ApiError(HttpStatus.OK, LocalDateTime.now().format(formatter),
                getLocalizeMessage(exception.getMessage(), locale));
        return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.OK);
    }

}
