package com.karakaya.permissionsystem.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LeaveRequest {

    @NotNull
    public LocalDate fromDate;

    @NotNull
    public LocalDate toDate;

    @NotNull
    public String leaveType;

    public String message;
}
