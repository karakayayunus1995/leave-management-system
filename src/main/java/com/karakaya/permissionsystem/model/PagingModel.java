package com.karakaya.permissionsystem.model;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class PagingModel<T> {

    private Long totalItems;

    private int totalPage;

    private int currentPage;

    List<T> data = new ArrayList<>();
}
