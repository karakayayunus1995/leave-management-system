package com.karakaya.permissionsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.karakaya.permissionsystem.entity.Leave;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class UserDto {

    private Long id;
    private String role;
    private String username;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDate jobStartDate;
    private int totalLeave;
    private List<Leave> leaves = new ArrayList<>();
    private String token;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDateTime createdDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private LocalDateTime modifiedDate;
}
