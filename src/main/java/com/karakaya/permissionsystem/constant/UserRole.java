package com.karakaya.permissionsystem.constant;

public enum UserRole {
    ADMIN,
    EMPLOYEE
}
