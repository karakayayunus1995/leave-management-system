package com.karakaya.permissionsystem.constant;

public enum LeaveStatus {
    PENDING,
    REJECTED,
    ACCEPTED
}
