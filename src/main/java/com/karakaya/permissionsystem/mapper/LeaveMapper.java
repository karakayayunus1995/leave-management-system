package com.karakaya.permissionsystem.mapper;

import com.karakaya.permissionsystem.entity.Leave;
import com.karakaya.permissionsystem.model.LeaveRequest;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LeaveMapper {

    LeaveMapper INSTANCE = Mappers.getMapper(LeaveMapper.class);

    Leave requestToLeave(LeaveRequest leaveRequest);
}
