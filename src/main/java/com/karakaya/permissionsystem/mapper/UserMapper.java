package com.karakaya.permissionsystem.mapper;

import com.karakaya.permissionsystem.entity.User;
import com.karakaya.permissionsystem.model.UserRequest;
import com.karakaya.permissionsystem.model.dto.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    User requestToUser(UserRequest userRequest);

    UserDto userToDto(User user);
}
