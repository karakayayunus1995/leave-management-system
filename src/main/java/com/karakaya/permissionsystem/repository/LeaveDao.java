package com.karakaya.permissionsystem.repository;

import com.karakaya.permissionsystem.entity.Leave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface LeaveDao extends JpaRepository<Leave, Long>, JpaSpecificationExecutor<Leave> {
}
