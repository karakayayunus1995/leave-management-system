package com.karakaya.permissionsystem.repository;

import com.karakaya.permissionsystem.entity.PublicHoliday;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PublicHolidayDao extends JpaRepository<PublicHoliday, Long> {
}
