package com.karakaya.permissionsystem.specification;

import com.karakaya.permissionsystem.entity.Leave;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class LeaveSpecification implements Specification<Leave> {

    public LeaveSpecification(String status) {
        this.status = status;
    }

    @Getter
    @Setter
    private String status;

    @Override
    public Predicate toPredicate(Root<Leave> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return criteriaBuilder.equal(root.get("status"), this.status);
    }
}
